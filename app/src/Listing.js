import React from 'react'

export default function Listing({ listing }) {
    console.log(listing)
    return (
        <div>
            <h2>{listing.price}</h2>
            <h3>{listing.address}</h3>
            <ul>{listing.features.map((feature, i) => <li key={i}>{feature}</li>)}</ul>
            <img src={listing.floorPlan} />
        </div>
    )
}
