import React, { useState, useEffect } from 'react'
import './App.css'
import Listing from './Listing'

export default function App() {
  const [listings, setListings] = useState([{ "propertyId": "5039481", "address": "King Square, Islington, London EC1V", "price": "Â£500,000", "pricePerFt": null, "tel": "02080332002", "features": ["2 bedrooms", "1 bathroom", "1 reception room"], "views": ["90 page views", "90 page views"], "priceHistory": [{ "date": "10th May 2019", "detail": "First listed", "price": "Â£500,000" }], "images": ["https://lid.zoocdn.com/645/430/127a70ce25f45eedba3cf725da24eb6dc3495afc.jpg", "https://lid.zoocdn.com/645/430/c0e561715dff802cfc5bd530a828cc757a57e009.jpg", "https://lid.zoocdn.com/645/430/38b504b3e110b3a3a2bc385f3460103c4722ee65.jpg", "https://lid.zoocdn.com/645/430/e47d74591e1abc1f44c2122baa32484b2e5147ff.jpg", "https://lid.zoocdn.com/645/430/de537873d3286ce5344b30ed27c60aa5d70c3897.jpg", "https://lid.zoocdn.com/645/430/8441802deededbfb739eee64fbdb8d0152d243f5.jpg", "https://lid.zoocdn.com/645/430/82cedf406fcaeb38274ad5d8931d8ec639b45dbc.jpg", "https://lid.zoocdn.com/645/430/d97ecd7f81f6870d08beab9a9867d9d5c0dd58f9.jpg", "https://lid.zoocdn.com/645/430/127a70ce25f45eedba3cf725da24eb6dc3495afc.jpg"], "floorPlan": "https://lc.zoocdn.com/21dc222fcf9e714ffddfcc66921ae8081b11d03c.jpg", "latLng": { "lat": "51.527986", "lng": "-0.098883" } }, { "propertyId": "5071796", "address": "King Square, Islington, London EC1V", "price": "Â£500,000", "pricePerFt": null, "tel": "02080332002", "features": ["2 bedrooms", "1 bathroom", "1 reception room"], "views": ["90 page views", "90 page views"], "priceHistory": [{ "date": "10th May 2019", "detail": "First listed", "price": "Â£500,000" }], "images": ["https://lid.zoocdn.com/645/430/127a70ce25f45eedba3cf725da24eb6dc3495afc.jpg", "https://lid.zoocdn.com/645/430/c0e561715dff802cfc5bd530a828cc757a57e009.jpg", "https://lid.zoocdn.com/645/430/38b504b3e110b3a3a2bc385f3460103c4722ee65.jpg", "https://lid.zoocdn.com/645/430/e47d74591e1abc1f44c2122baa32484b2e5147ff.jpg", "https://lid.zoocdn.com/645/430/de537873d3286ce5344b30ed27c60aa5d70c3897.jpg", "https://lid.zoocdn.com/645/430/8441802deededbfb739eee64fbdb8d0152d243f5.jpg", "https://lid.zoocdn.com/645/430/82cedf406fcaeb38274ad5d8931d8ec639b45dbc.jpg", "https://lid.zoocdn.com/645/430/d97ecd7f81f6870d08beab9a9867d9d5c0dd58f9.jpg", "https://lid.zoocdn.com/645/430/127a70ce25f45eedba3cf725da24eb6dc3495afc.jpg"], "floorPlan": "https://lc.zoocdn.com/21dc222fcf9e714ffddfcc66921ae8081b11d03c.jpg", "latLng": { "lat": "51.527986", "lng": "-0.098883" } }])

  useEffect(() => {
    async function fetchListings() {
      const result = await fetch('http://localhost:3001/get-listings')
      const data = await result.json()
      setListings(data)
    }
    fetchListings()
  }, [])

  return (
    <div className="App">
      <h1>listings</h1>
      {listings.map(listing => <Listing key={listing.propertyId} listing={listing}>{listing}</Listing>)}
    </div>
  )
}
