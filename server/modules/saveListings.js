const fs = require('fs')
const moment = require('moment')
const listingPath = 'listings/'

async function saveListings(listings) {
    const filename = moment().format('YYYY-MM-DD-HH-mm')
    fs.writeFile(`${listingPath}${filename}.json`, JSON.stringify(listings), () => { })
}

module.exports = saveListings
