require('dotenv').config()
const puppeteer = require('puppeteer')

const loginUrl = 'https://www.zoopla.co.uk/signin/'
const userInput = '#signin_email'
const passInput = '#signin_password'
const submit = '#signin_submit'

async function login() {
    const browser = await puppeteer.launch({ headless: false })
    const page = await browser.newPage()
    await page.goto(loginUrl)
    await page.type(userInput, process.env.ZOOPLA_USER)
    await page.type(passInput, process.env.ZOOPLA_PASS)
    await page.click(submit)
    await page.waitForNavigation()
}

login()
