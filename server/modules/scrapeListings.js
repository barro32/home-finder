const puppeteer = require('puppeteer')
const fs = require('fs').promises
const allUrls = require('../data/urls.json')

async function setup(urls) {
    const browser = await puppeteer.launch({ headless: true })

    const scrape = async url => {
        const propertyId = url.replace('https://www.zoopla.co.uk/for-sale/details/', '')
        const page = await browser.newPage()
        await page.goto(url)
        return await page.evaluate((propertyId) => {
            const expired = window.location.hash === '#expired'
            if (expired) return { propertyId }
            const address = document.querySelector('.ui-property-summary__address').textContent.trim()
            const price = document.querySelector('.ui-pricing__main-price').textContent.trim()
            const pricePerFtEl = document.querySelector('.ui-pricing__area-price')
            const pricePerFt = pricePerFtEl ? pricePerFtEl.textContent.trim() : null
            const tel = document.querySelector('.ui-agent__tel a').href.replace('tel:', '')
            const agent = document.querySelector('.ui-agent__name').textContent
            const features = [...document.querySelectorAll('.dp-features-list__text')].map(el => el.textContent.trim())
            const views = [...document.querySelectorAll('.dp-view-count__value')].map(el => el.textContent.trim())
            const priceHistory = [...document.querySelectorAll('.dp-price-history__item')].map(el => ({
                'date': el.querySelector('.dp-price-history__item-date').textContent.trim(),
                'detail': el.querySelector('.dp-price-history__item-detail').textContent.trim(),
                'price': el.querySelector('.dp-price-history__item-price').textContent.trim()
            }))
            const images = [...document.querySelectorAll('.dp-gallery__image')].map(img => img.src)
            const floorPlanEl = document.querySelector('#floorplan-1')
            const floorPlan = floorPlanEl ? floorPlanEl.querySelector('.ui-modal-gallery__asset').style.backgroundImage.replace('url("', '').replace('")', '') : ''
            const geo = [...document.querySelectorAll('script[type="application/ld+json"]')].map(s => JSON.parse(s.textContent)).filter(s => s['@graph'])[0]['@graph'].filter(obj => obj['@type'] === 'Residence')[0].geo
            const latLng = { 'lat': geo.latitude, 'lng': geo.longitude }

            await page.close()
            return { propertyId, address, price, pricePerFt, agent, tel, features, views, priceHistory, images, floorPlan, latLng }
        }, propertyId)
    }

    await Promise.all(urls.map(url => scrape(url).then(data => {
        fs.writeFile(`listings/${data.propertyId}.json`, JSON.stringify(data))
    }).catch(console.error)))
    await browser.close()
}

// const randomUrls = allUrls.sort(() => .5 - Math.random()).slice(0, 25)
setup(allUrls)
