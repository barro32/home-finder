const express = require('express')
const fs = require('fs').promises
const cors = require('cors')
const app = express()
const port = 3001

app.use(cors())

app.get('/get-listings', async (req, res) => {
    const files = await fs.readdir('./listings/')
    const listings = await Promise.all(files.map(async file => await fs.readFile(`./listings/${file}`, 'utf-8').then(file => JSON.parse(file))))
    res.json(listings)
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
